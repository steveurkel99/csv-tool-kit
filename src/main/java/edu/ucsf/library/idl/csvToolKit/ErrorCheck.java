package edu.ucsf.library.idl.csvToolKit;


import java.util.ArrayList;
import java.util.Arrays;

/**
 * handles both finding errors and verifying the integrity of the file as well
 * as printing out (using fileHandler class) clean and error-ridden lines of
 * metadata
 * 
 * 
 * @author Michael Antoun
 *
 */

public class ErrorCheck {
	public static Mode mode = null;

	/**
	 * takes parameters regarding whether or not the user is looking for
	 * error-ridden or clean files (or both) and then finds which lines have
	 * errors and either prints them or doesnt
	 * 
	 * @param file
	 * @param clean
	 * @param dirty
	 */
	public static void find(ArrayList<String> file, boolean clean, boolean dirty) {

		if (dirty) {
			mode = Mode.ERROR;
		}
		if (clean)
			mode = Mode.CLEAN;
		if (dirty && clean)
			mode = Mode.ALL;
		if (mode.equals(Mode.ERROR) || mode.equals(Mode.ALL)) {
			FileHandler.write("\"LINE NUMBER\", \"ERROR MESSAGE\", " + file.get(0) + "\r\n", mode);
		}

		for (String line : file) {
			checkLine(line, file, clean, dirty);
		}
	}

	/**
	 * returns true or false, representing whether or not the file is error-free
	 * 
	 * @param file
	 * @return
	 */

	public static boolean verify(ArrayList<String> file) {

		for (String line : file) {
			if (checkFieldErrorByNumber(line, file))
				return false;
			for (String field : processWords(line.split("\"" + Runner.DELIMETER + "\""))) {
				if (checkQuoteErrorByNumber(countQuotes(field)))
					return false;
			}
		}
		return true;
	}

	/**
	 * returns true if there is an error with the number of qutoes in a given
	 * field of metadata
	 * 
	 * @param numQuotes
	 * @return
	 */
	public static boolean checkQuoteErrorByNumber(int numQuotes) {
		if (numQuotes == 0)
			return false;
		if (numQuotes % 2 != 0 || (numQuotes - 2) % 4 != 0)
			return true;
		// ^^^^ number of double quotes (excluding outer pair) should be a
		// multiple of twice the regex used ^^^^
		return false;
	}

	/**
	 * 
	 * performs a field number and quote number check on the line of metadata
	 * and determines if it has an error or not, and of what type(s)
	 * 
	 * @param line
	 * @param file
	 * @param clean
	 * @param dirty
	 */

	public static void checkLine(String line, ArrayList<String> file, boolean clean, boolean dirty) {
		boolean quoteError = false;
		boolean fieldNumError = false;
		String quoteErrorField = "";
		for (String field : Arrays.asList(processWords(line.split("\"" + Runner.DELIMETER + "\"")))) {
			if (checkQuoteErrorByNumber(countQuotes(field))) {
				quoteError = true;
				String s = processWords(file.get(0).split("\"" + Runner.DELIMETER + "\""))[Arrays
						.asList(processWords(line.split("\"" + Runner.DELIMETER + "\""))).indexOf(field)];
				quoteErrorField = s.substring(1, s.length() - 1);
			}
		}
		if (checkFieldErrorByNumber(line, file))
			fieldNumError = true;
		if (clean && !(fieldNumError || quoteError) && !dirty) {
			System.out.println(line);
			FileHandler.write(line + "\r\n", mode);
			return;
		}
		if (clean && !(fieldNumError || quoteError) && dirty) {
			System.out.println("\"" + file.indexOf(line) + 1 + line);
			FileHandler.write("\"" + file.indexOf(line) + "\", \"\", " + line + "\r\n", mode);
			return;
		}
		if ((fieldNumError || quoteError) && dirty) {
			System.out.print(file.indexOf(line) + 1 + " ");
			FileHandler.write("\"" + (file.indexOf(line)) + "\", ", mode);
		}
		if (quoteError && dirty) {
			System.out.print("ERROR_NUM_QUOTES @ " + quoteErrorField + " ");
			FileHandler.write("\"ERROR_NUM_QUOTES @ " + quoteErrorField + "\", ", mode);
		}
		if (fieldNumError && dirty) {
			System.out.print("ERROR_NUM_FIELDS, EXPECTED: " + file.get(0).split("\"" + Runner.DELIMETER + "\"").length
					+ ", ACTUAL: " + line.split("\"" + Runner.DELIMETER + "\"").length);
			FileHandler.write("ERROR_NUM_FIELDS, EXPECTED: " + file.get(0).split("\"" + Runner.DELIMETER + "\"").length
					+ ", ACTUAL: " + line.split("\"" + Runner.DELIMETER + "\"").length, mode);
		}
		if ((fieldNumError || quoteError) && dirty) {
			System.out.println(line);
			FileHandler.write(line + "\r\n", mode);
		}
	}

	/**
	 * checks if metadata line has the correct number of fields
	 * 
	 * @param field
	 * @param file
	 * @return
	 */
	public static boolean checkFieldErrorByNumber(String field, ArrayList<String> file) {
		return !(field.split("\"" + Runner.DELIMETER + "\"").length == file.get(0)
				.split("\"" + Runner.DELIMETER + "\"").length);
	}

	/**
	 * checks if field has an error in number of double quotes
	 * 
	 * @param field
	 * @return
	 */
	public static boolean checkField(String field) {
		if (checkQuoteErrorByNumber(countQuotes(field))) {
			return true;
		}
		return false;
	}

	/**
	 * counts number of quotes in a given metadata field
	 * 
	 * @param s
	 * @return
	 */
	public static int countQuotes(String s) {
		int quotes = 0;
		for (char c : s.toCharArray()) {
			quotes += c == '\"' ? 1 : 0;
		}
		return quotes;
	}

	/**
	 * Taken from Test class, consistently places double quotes around split
	 * metadata fields
	 * 
	 * @param entries
	 * @return
	 */

	public static String[] processWords(String[] entries) {
		String[] copy = entries;
		copy[0] = entries[0] + "\"";
		for (int i = 1; i < copy.length - 1; i++) {
			copy[i] = "\"" + entries[i] + "\"";
		}
		copy[copy.length - 1] = "\"" + entries[copy.length - 1];
		return copy;
	}
}
