package edu.ucsf.library.idl.csvToolKit;

import java.util.ArrayList;

/**
 * replaces incorrect escape with correct one
 * exports fixed lines to a file
 * 
 * identifies which lines cannot be fixed, exports them to another file
 * 
 * 
 * @author Michael Antoun
 *
 */
public class Fix {
	
	/**
	 * only method in class, does everything
	 * 
	 * @param file
	 */
	
	public static void fix(ArrayList<String> file){
		int quoteRegex = FindEscape.find(file);
		if (quoteRegex == 0) quoteRegex = 2;
		for(String line : file){
			String copy = "";
			for(String s : ErrorCheck.processWords(line.split("\"" + Runner.DELIMETER + "\""))){
				copy += "\"" + LevelingFlattener.fix(s.substring(1, s.length()-1), quoteRegex) + "\"" + ",";
			}
			file.set(file.indexOf(line), copy.substring(0, copy.length()-1));
		}
		for(String line : file){
			file.set(file.indexOf(line), line.replaceAll(LevelingFlattener.stringMultiply("\"", quoteRegex), "\"\""));
		}
		FileHandler.write("\"LINE NUMBER\", \"ERROR MESSAGE\", " + file.get(0) + "\r\n", Mode.UNFIXABLE);
		for(String line : file){
			ArrayList<String> a = new ArrayList<String>();
			a.add(line);
			if (ErrorCheck.verify(a)){
				ErrorCheck.mode = Mode.FIX;
				ErrorCheck.checkLine(line, file, true, false);
			}
			else{
				ErrorCheck.mode = Mode.UNFIXABLE;
				ErrorCheck.checkLine(Middleman.fileToFixedList(Runner.inputDirectory).get(file.indexOf(line)), file, false, true);
			}
		}
		
	}
	
	
}
