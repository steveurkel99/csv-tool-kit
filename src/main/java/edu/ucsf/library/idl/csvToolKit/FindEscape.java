package edu.ucsf.library.idl.csvToolKit;

import java.util.ArrayList;

/**
 * method finds what escape the file is using
 * this could mean too many regex, proper regex, or no regex at all
 * 
 * @author Michael Antoun
 *
 */
public class FindEscape {
	
	/**
	 * takes metaData file as an ArrayList of lines
	 * returns guess of what escape is used based on most common number of leading quotes seen on any line
	 * 
	 * @param file
	 * @return number of quotes used to represent a double quote in the file
	 *        i.e. --- """ used to represent " would return 3
	 */
	public static int find(ArrayList<String> file){
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (String s : file){
			s = s.replaceFirst("\"", "");
			if (countQuotes(s) != 0) list.add(countQuotes(s));	
		}
		return findMode(list); //mode of arraylist
	}
	
	/**
	 * used with above method to find number of leading (")s and use this to predict what the escape is for the file
	 * 
	 * @param a metaData line
	 * @return number of leading (")s
	 */
	public static int countQuotes(String s){
		int count = 0;
		while(s.charAt(0) == '\"'){ //continues to run until no more leading (")s exist
			count++;
			s = s.substring(1);
		}
		return count;
	}
	
	/**
	 * returns the mode of a given arraylist data set
	 * 
	 * @param list
	 * @return mode
	 */
	
	public static int findMode(ArrayList<Integer> list){
		ArrayList<Integer> counts = new ArrayList<Integer>();
		for (int i = 0; i < 100; i++) counts.add(0);
		for (int i : list){
			counts.set(i, counts.get(i) + 1);
		}
		int max = 0;
		for (int count : counts){
			if (count>max)
				max = count;
		}
		return counts.indexOf(max);
	}
	
	
	
	
	
	/**
	 * created only for testing purposes
	 * @param args
	 */
	public static void main(String[] args){
		ArrayList<String> a = FileHandler.fileToArrayList("src/RJR_Documents_Added.txt");
		System.out.println(find(a));
		
	}
}
