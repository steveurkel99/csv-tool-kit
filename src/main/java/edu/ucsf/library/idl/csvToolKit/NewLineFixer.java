package edu.ucsf.library.idl.csvToolKit;

import java.util.ArrayList;

public class NewLineFixer {
	public static ArrayList<String> fixer(ArrayList<String> list){
		int correct = getNumFields(list.get(0));
		for(int i = 1; i < list.size(); i++){
			if (getNumFields(list.get(i)) < correct)
				list = stringAppender(i, list, correct);
		
		}
		return list;
	}
	
	public static ArrayList<String> stringAppender(int i, ArrayList<String> list, int correct){
		ArrayList<String> copy = list;		
		while(getNumFields(copy.get(i)) < correct && i < copy.size()-1){
			copy.set(i, copy.get(i).substring(0,copy.get(i).length()-1) + copy.get(i + 1).substring(1));
			copy.remove(i+1);
		}
		
		if (getNumFields(copy.get(i)) == correct)
			return copy;
	
		return list;
	}
	
	
	public static int getNumFields(String s){
		return s.split("\"" + Runner.DELIMETER + "\"").length;
	}
}
