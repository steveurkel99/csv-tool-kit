package edu.ucsf.library.idl.csvToolKit;

/**
 * Final step of double quote fixing mechanism
 * Takes flattened metaData fields (only error is consistently wrong escape for quotes)
 * methods inside work to return a properly formatted metaData field
 * 
 * @author Michael Antoun
 *
 */

public class EscapeFixer {
	
	final static String CORRECT_QUOTES = LevelingFlattener.stringMultiply("\"", 2); // initializing correct escape
	
	/**
	 * 
	 * @param metaDataField
	 * @param wrongEscape
	 * @return field of metaData with quotes adjusted to fit CSV conventions
	 */
	public static String fix(String metaDataField, int wrongEscape){
		String wrongQuotes = LevelingFlattener.stringMultiply("\"", wrongEscape);
		String s = metaDataField.substring(1, metaDataField.length()-1); // takes quotes off
		s = s.replaceAll(wrongQuotes, CORRECT_QUOTES); // changes escape to what it should be (CSV format)
		return "\"" + s + "\"";  // adds quotes back to string
	}
}
