package edu.ucsf.library.idl.csvToolKit;

public enum Mode{
	ERROR("Error", "e"), 
	CLEAN("Clean", "c"),
	ALL("All", "a"),
	FIX("Fix", "f"),
	UNFIXABLE("Unfixable", "f"),
	RECORDS("Records", "r"),
	EXTRACT("Extract", "x"),
	SPLIT("Split", "s");

	private String name;
	private String command;
	
	private Mode(String name, String command ) {
	        this.name = name;
	        this.command = command;
	    }
	
	public String getCommand(){
		return this.command;
	}
	public String toString() {
	    return name;
	}

}
