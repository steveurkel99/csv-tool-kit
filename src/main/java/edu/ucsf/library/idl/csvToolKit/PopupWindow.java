package edu.ucsf.library.idl.csvToolKit;


import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Used to make the newly created files pop up as a new window so the user can
 * easily and quickly view them
 * 
 * @author Michael Antoun
 *
 */

public class PopupWindow {

	/**
	 * performs the popup action using Desktop class
	 * 
	 * ***only tested on windows, not sure if it'll work on mac***
	 * 
	 * @param mode
	 * @throws URISyntaxException
	 */
	public static void popup(Mode mode) {
		try {
			Desktop.getDesktop()
					.edit(new File(FileHandler.createDir(mode)));
			// URL url = new URL("https://www.youtube.com/watch?v=dQw4w9WgXcQ");
			// Desktop.getDesktop().browse(url.toURI());
			// System.out.println("\n\nLOL u've been rick rolled");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
