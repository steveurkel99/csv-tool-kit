package edu.ucsf.library.idl.csvToolKit;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * 
 * provides basic information from a given file
 * data includes: number of lines, delimeter, field headers
 * 
 * @author Michael Antoun
 *
 */
public class FileInformation {
	public static String USER_DELIMETER = null;
	/**
	 * constructs a string using information from the file to create a 
	 * well-formatted message to be printed to the console
	 * 
	 * @param file
	 * @return
	 */
	public static String info(ArrayList<String> file){
		String s = "\nFIELDS: \n\n";
		s += file.get(0) + "\n";
		int counter = 1;
		for (String field : file.get(0).split("\"" + findDelimeter(file.get(0)) + "\"")){
			s += counter + ": " + field.replaceAll("\"", "") + "\n";
			counter++;
		}
		s += "\nDATA ROWS: " + (file.size()-1);
		s += "\n\n" + ((USER_DELIMETER != null)? "USER INPUT DELIMETER: " : "DELIMETER DETECTED: ")+ findDelimeter(file.get(0));
		s += "\n--------------------------";
		Runner.DELIMETER = findDelimeter(file.get(0));
		return s;
	}
	
	/**
	 * finds which delimeter shows up the most (comma, tab, or pipe)
	 * whichever shows up most is determined to be the delimeter
	 * 
	 * @param line
	 * @return
	 */
	
	public static String findDelimeter(String line){
		if (USER_DELIMETER != null) return  USER_DELIMETER;
		int comma = findOccurances(line, ",");
		int pipe = findOccurances(line, "|");
		int tab = findOccurances(line, "\t");
		int winner = Math.max(comma, Math.max(pipe, tab));
		if (winner == comma) return ",";
		if (winner == pipe) return "|";
		if (winner == tab) return "\t";
		return "Cannot Determine";
	}
	
	/**
	 * used by FindDelimeter to find how many occurances of each 
	 * possible delimeter there are
	 * 
	 * @param big
	 * @param sub
	 * @return
	 */
	
	public static int findOccurances(String big, String sub){
		int count = 0;
		while(big.contains(sub)){
			count++;
			big = big.replaceFirst(sub, "");

		}
		return count;
	}
	
	/**
	 * created only for testing purposes, never utilized
	 * 
	 * @param args
	 */
	
	public static void main(String[] args){
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader("src/RJR_Documents_Added.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String line;
		ArrayList<String> metaDataLines = new ArrayList<String>();
		try {
			while((line = br.readLine()) != null ){
				metaDataLines.add(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(info(metaDataLines));
	}
}
