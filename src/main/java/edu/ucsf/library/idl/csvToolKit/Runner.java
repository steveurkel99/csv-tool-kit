package edu.ucsf.library.idl.csvToolKit;


import java.util.Arrays;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * contains main method, calls all other methods
 * 
 * contains logic for options stufff (handling user args[])
 * 
 * 
 * @author Michael Antoun
 *
 */

public class Runner {
	public static String DELIMETER = ",";
	public static String inputDirectory = null;
	public static String outputDirectory = null;
	public static CommandLine cl = null;
	public static boolean pop;
	
	public static void main(String[] args) throws NullPointerException{
		System.err.println(Arrays.toString(args));
		Options options = new Options();
		options.addOption("e", "error", false, "exports all lines with errors to new file");
		options.addOption("i", "information", false,
				"displays statistics of metadata file (columns, number of lines, delimeter");
		options.addOption("c", "clean", false, "exports all correctly formatted lines to a new file");
		options.addOption("d", "set-delimeter", true, "sets the delimeter to be used for processing the file");
		options.addOption("I", "input-file", true, "sets the file to be dealt with (taken as directory)");
		options.addOption("a", "all", false, "exports all lines to file, bad lines marked with error message");
		options.addOption("O", "output-directory", true, "sets output directory");
		options.addOption("f", "fix", false,
				"fixes fixable lines and exports them to a file, exports second file with unfixable lines");
		options.addOption("x", "extract-column", true, "exports all columns by name given by user, separated by comma");
		options.addOption("r", "extract-records", true,
				"exports takes argument (column name) = (value), returns all records that meet condition [MUST BE SURROUNDED BY DOUBLE QUOTE]");
		options.addOption("v", "verify-integrity", false,
				"tells whether all files in document conform to RCF 4180 or not");
		options.addOption("p", "popup", false, "opens all created text files in default text editor");
		options.addOption("s", "split", true, "splits a column into two columns at a specified delimeter. FORMAT: oldColumn!delimeter!newColumn!newColumn2    (parentheses are the delimeter for the arguments");

		CommandLineParser parser = new DefaultParser();
	
		
		try {
			cl = parser.parse(options, args);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (cl.hasOption("d")) {
			DELIMETER = cl.getOptionValue("d");
			FileInformation.USER_DELIMETER = cl.getOptionValue("d");
			System.out.println("Delimeter set to " + FileInformation.USER_DELIMETER);
		}
		 pop = false;
		if (cl.hasOption("p")) {
			pop = true;
		}
		if (cl.hasOption("I")) {
			inputDirectory = cl.getOptionValue("I");
		} else {
			System.out.println("ERROR: You need an input file to work with");
			return;
		}
		if (cl.hasOption("i")) {
			System.out.println(FileInformation.info(Middleman.fileToFixedList(inputDirectory)));
		}
		if (cl.hasOption("v")) {
			if (ErrorCheck.verify(FileHandler.fileToArrayList(inputDirectory)))
				System.out.println("File is all according to RCF 4180 standards");
			else
				System.out.println("File has at least one error");
		}
		if (cl.hasOption("O")) {
			outputDirectory = cl.getOptionValue("O");
		}
		if (cl.hasOption("s")){
			if (outputDirectory == null)
				System.out.println("ERROR: must supply output directory for files");
			FileHandler.initialize(Mode.SPLIT);
			ColumnSplitter.split(cl.getOptionValue("s"));
			FileHandler.close(Mode.SPLIT);
			if (pop)
				PopupWindow.popup(Mode.SPLIT);
			
		}
		if (cl.hasOption("e")) {
			if (outputDirectory == null){
				System.out.println("ERROR: must supply output directory for files");
				return;
			}
			FileHandler.initialize(Mode.ERROR);
			ErrorCheck.find(FileHandler.fileToArrayList(inputDirectory), false, true);
			FileHandler.close(Mode.ERROR);
			if (pop)
				PopupWindow.popup(Mode.ERROR);
		}
		if (cl.hasOption("c")) {
			if (outputDirectory == null)
				System.out.println("ERROR: must supply output directory for files");
			FileHandler.initialize(Mode.CLEAN);
			ErrorCheck.find(FileHandler.fileToArrayList(inputDirectory), true, false);
			if (pop)
				PopupWindow.popup(Mode.CLEAN);
		}
		if (cl.hasOption("a")) {
			if (outputDirectory == null){
				System.out.println("ERROR: must supply output directory for files");
				return;
			}
			FileHandler.initialize(Mode.ALL);
			ErrorCheck.find(Middleman.fileToFixedList(inputDirectory), true, true);
			if (pop)
				PopupWindow.popup(Mode.ALL);
		}

		if (cl.hasOption("x")) {
			if (outputDirectory == null){
				System.out.println("ERROR: must supply output directory for files");
				return;
			}
			FileHandler.initialize(Mode.EXTRACT);
			String[] columns = cl.getOptionValue("x").trim().split(",");
			Extracter.getColumns(Middleman.fileToFixedList(inputDirectory), columns);
			FileHandler.close(Mode.EXTRACT);
			if (pop)
				PopupWindow.popup(Mode.EXTRACT);
		}
		if (cl.hasOption("r")) {
			if (outputDirectory == null)
				System.out.println("ERROR: must supply output directory for files");
			System.out.println(cl.getOptionValue("r"));
			FileHandler.initialize(Mode.RECORDS);
			
			String s = cl.getOptionValue("r").replaceAll(":", "=");
			
			Extracter.getRecords(Middleman.fileToFixedList(inputDirectory), s);
			FileHandler.close(Mode.RECORDS);
			if (pop)
				PopupWindow.popup(Mode.RECORDS);
		}
		if (cl.hasOption("f")) {
			if (outputDirectory == null)
				System.out.println("ERROR: must supply output directory for files");
			FileHandler.initialize(Mode.FIX);
			FileHandler.initialize(Mode.UNFIXABLE);
			Fix.fix(Middleman.fileToFixedList(inputDirectory));
			FileHandler.close(Mode.FIX);
			FileHandler.close(Mode.UNFIXABLE);
			if (pop)
				PopupWindow.popup(Mode.FIX);
			if (pop)
				PopupWindow.popup(Mode.UNFIXABLE);
		}

	}

}
