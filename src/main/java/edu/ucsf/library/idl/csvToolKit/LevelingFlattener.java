package edu.ucsf.library.idl.csvToolKit;

import java.util.Arrays;
/**
 * takes incorrect metaData field and the incorrect escape it's using, and corrects it to its own standards
 * leveling refers to inner-quotes in some metaData files being escaped with more quotes than inner ones
 * can take files that are consistently incorrect (using a different convention)
 * 
 * @author Michael Antoun
 *
 */


public class LevelingFlattener {
	
	/**
	 * 
	 *leveling refers to inner-quotes in some metaData files being escaped with more quotes than inner ones
	 * also fixes inadvertent extra quotes
	 * 
	 * LOGIC - splits String into array of individual letters and sequences of (")s
	 * reconstructs string with sequences of (")s deprecated to nearest multiple of known escape used in file
	 * 
	 * @param metaDataField
	 * @param quoteRegex
	 * @return a flattened version of the metaDataField string, without leveling
	 */
	public static String fix(String metaDataField, int quoteRegex){
		String regex = "(?=[^\"])|(?<=[^\"])"; //uses 0-length lookahead to keep delimiter in resulting string
		String[] splitter = metaDataField.split(regex); 
		String copy = "";
		for (String s : splitter){
			if (s.contains("\"")&&s.length()>=quoteRegex)
				copy += stringMultiply("\"", (s.length()/quoteRegex)*quoteRegex);
			else
				copy += s;
		}
		return copy;
	}
	
	/**
	 * convenience method, same operation as * for string repetition in groovy
	 * 
	 * @param s
	 * @param times
	 * @return string repetition of parameter string
	 */
	public static String stringMultiply(String s, int times){
		String a = "";
		for (int i = 0; i < times; i ++){
			a += s;
		}
		return a;
	}
	
	/**
	 * created only for testing purposes
	 * @param args
	 */
	public static void main(String[] args){
		System.out.println("\"\"\"\"\"\"\"LIGHT MY LUCKY\"\"\"\" LUCKY STRIKE.\"\"\"\"");

		System.out.println(fix("\"\"\"\"\"\"\"LIGHT MY LUCKY\"\"\"\" LUCKY STRIKE.\"\"\"\"", 3));
		System.out.println(Arrays.toString("\"\"\"\"\"\"\"LIGHT MY LUCKY\"\"\"\" LUCKY STRIKE.\"\"\"\"".split("[a-zA-Z0-9']")));
	}
}