package edu.ucsf.library.idl.csvToolKit;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
/**
 * Not used for anything in program, first draft of code.
 * 
 *  some methods taken and used in actual program
 * 
 * DEPRECATED
 * 
 * @author Michael Antoun
 *
 */

@Deprecated

public class Test {
	public static void main(String[] args) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader("src/RJR_Documents_Added.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String line;
		ArrayList<String> metaDataLines = new ArrayList<String>();
		try {
			while((line = br.readLine()) != null ){
				metaDataLines.add(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the separation character (t for tab)");
		String regex = scan.nextLine();
		System.out.println("Enter the expected number of quotes used as quote escape");
		int expectedQuotes = scan.nextInt();
		
		System.out.println(checkMetaDataBlock(metaDataLines, regex, expectedQuotes));
		
		

	}
	
	public static String[] SplitByRegex(String data, String regex){
		return processWords(data.split("\"\\" + regex + "\""));
	}
	
	/**
	 * counts quotes in given line
	 * @param s
	 * @return
	 */
	public static int getDoubleQuoteCount(String s){
		int quotes = 0;
		for (char c : s.toCharArray()){
			quotes += c=='\"'? 1:0;
		}
		return quotes;
	}
	
	/**
	 * simple modulus test to see if field matches CSV standards
	 * @param numQuotes
	 * @param expectedRegex
	 * @return
	 */
	public static boolean checkQuoteErrorByNumber(int numQuotes, int expectedRegex){
		if (numQuotes == 0) return false;
		if (numQuotes%2 != 0 || (numQuotes-2)%(expectedRegex*2) != 0){
			 System.out.println("ERROR"); 
			 return true;
		}
		// ^^^^ number of double quotes (excluding outer pair) should be a multiple of twice the regex used ^^^^
		return false;
		
	}
	/**
	 * prints data 
	 * prints null for empty field
	 * @param s
	 */
	public static void printData(String s){
		if (s.equals("\"\"")){
			System.out.println("null");
			return;
		}
		System.out.println(s);
	}
	
	/**
	 * Calls other metaData checker in a loop ¯\_(ツ)_/¯
	 * 
	 * @param allData
	 * @param regex
	 * @param expectedNumQuotes
	 * @return
	 */
	public static String checkMetaDataBlock(ArrayList<String> allData, String regex, int expectedNumQuotes){
		for (String metaData : allData){
			if (checkMetaDataLine(metaData, regex, expectedNumQuotes).equals("Doesn't Work"))
					return "Doesn't work; error at line " + (allData.indexOf(metaData) + 1);
		}
		return "Works";
	}
	
	/**
	 * 
	 * Checks for errors the old way (modulus arithmetic on double quote count)
	 * Deprecated
	 * 
	 * @param data
	 * @param regex
	 * @param expectedRegex
	 * @return
	 */
	
	public static String checkMetaDataLine(String data, String regex, int expectedRegex){
		String[] entries = SplitByRegex(data, regex);
		System.out.println(data);
		for (String s : entries){
			printData(s);
			int numQuotes = getDoubleQuoteCount(s);
			if (checkQuoteErrorByNumber(numQuotes, expectedRegex)){
				return "Doesn't Work";
			}
		}
		return "Works";
	}
	
	/**
	 * puts double quotes consistently around all metadata fields
	 * 
	 * copied into other class to keep this one deprecated
	 * 
	 * 
	 * @param entries
	 * @return
	 */
	
	public static String[] processWords(String[] entries){
		String[] copy = entries;
		copy[0] = entries[0] + "\""; 
		for (int i = 1; i < copy.length-1; i++){
			copy[i] = "\"" + entries[i] + "\""; 
		}
		copy[copy.length-1] = "\"" + entries[copy.length-1]; 
		return copy;
	}
	

}







