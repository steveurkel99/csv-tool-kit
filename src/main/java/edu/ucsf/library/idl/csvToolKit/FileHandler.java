package edu.ucsf.library.idl.csvToolKit;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class that handles all methods relating to writing or reading a file
 * 
 * used for organization,, consolidates all try catch statements in one place
 * 
 * @author Michael Antoun
 *
 */

public class FileHandler {
	static BufferedWriter cleanBW = null, errorBW = null, fixBW = null, allBW = null, extractBW = null, recordBW = null,
			unfixableBW = null, splitBW = null;

	/**
	 * converts file to an arraylist of lines
	 * 
	 * 
	 * @param inputDirectory
	 * @return an arraylist containing the lines separated as elements
	 */
	public static ArrayList<String> fileToArrayList(String inputDirectory) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(inputDirectory));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String line;
		ArrayList<String> metaDataLines = new ArrayList<String>();
		try {
			while ((line = br.readLine()) != null) {
				metaDataLines.add(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return metaDataLines;

	}

	/**
	 * 
	 * specifies file name (as an extension to the given file path)
	 * 
	 * Creates a bufferedWriter object and points the static variable name
	 * corresponding to the mode to that object
	 * 
	 * @param directory
	 *            for output file
	 * @param mode
	 */
	public static void initialize(Mode mode) {
		BufferedWriter bw = null;
		String dir = createDir(mode);
		try {
			bw = new BufferedWriter(new FileWriter(dir));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		switch (mode) {
		case ERROR:
			errorBW = bw;
			break;
		case CLEAN:
			cleanBW = bw;
			break;
		case ALL:
			allBW = bw;
			break;
		case EXTRACT:
			extractBW = bw;
			break;
		case RECORDS:
			recordBW = bw;
			break;
		case SPLIT:
			splitBW = bw;
			break;
		case FIX:
			fixBW = bw;
		case UNFIXABLE:
			unfixableBW = bw;
			break;
		}

	}

	/**
	 * 
	 * sets the bufferedWriter object equal to an initialized static object
	 * then, writes to that file
	 * 
	 * 
	 * @param content
	 *            to be written to file
	 * @param mode
	 */

	public static void write(String content, Mode mode) {
		BufferedWriter bw = null;
		switch (mode) {
		case ERROR:
			bw = errorBW;
			break;
		case CLEAN:
			bw = cleanBW;
			break;
		case ALL:
			bw = allBW;
			break;
		case SPLIT:
			bw = splitBW;
			break;
		case EXTRACT:
			bw = extractBW;
			break;
		case RECORDS:
			bw = recordBW;
			break;
		case FIX:
			bw = fixBW;
			break;
		case UNFIXABLE:
			bw = unfixableBW;
			break;
		default:
			System.out.println("Wrong mode");
			return;
		}

		try {
			bw.write(content);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * closes the bufferedWriter, MUST BE DONE FOR CONTENT TO BE WRITTEN
	 * 
	 * Doesn't close FileWriter, doesn't seem to cause any problems for now
	 * 
	 * @param mode
	 */

	public static void close(Mode mode) {
		BufferedWriter bw = null;
		switch (mode) {
		case ERROR:
			bw = errorBW;
			break;
		case CLEAN:
			bw = cleanBW;
			break;
		case ALL:
			bw = allBW;
			break;
		case EXTRACT:
			bw = extractBW;
			break;
		case RECORDS:
			bw = recordBW;
			break;
		case SPLIT:
			bw = splitBW;
			break;
		case FIX:
			bw = fixBW;
			break;
		case UNFIXABLE:
			bw = unfixableBW;
			break;
		default:
			System.out.println("Wrong mode");
			return;
		}
		try {
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Creates a directory for a File name to be initialized, found to be
	 * convenient as a static method rather than duplicated in multiple places
	 * 
	 * Adds long name of command added to output directory, handles Extract and
	 * Records exceptions: *** Records: "=" goes away, camel case applied to
	 * field *** Extract: camel case applied, fields separated with underscore
	 * ("_")
	 * 
	 * @param mode
	 * @return
	 */
	public static String createDir(Mode mode) {
		String pathSep = System.getProperty("file.separator");
		String dir = Runner.outputDirectory;
		System.out.println(pathSep);
		dir += dir.endsWith(pathSep) ? "" : pathSep;
		String s = Runner.inputDirectory;
		dir += s.substring(s.lastIndexOf(pathSep), s.lastIndexOf("."));
		dir += "_" + mode;
		if (mode.equals(Mode.EXTRACT) || mode.equals(Mode.RECORDS)) {
			dir += "_";
			String temp = Runner.cl.getOptionValue(mode.getCommand()).replaceAll(",", "_").replaceAll("\"", "").toLowerCase();
			if (temp.contains("="))
				temp = temp.substring(0, temp.indexOf("="));
			while (temp.contains("\\s[a-zA-Z]")) {
				temp = temp.replaceFirst("\\s[a-zA-Z]",
						Character.toString((char) (temp.charAt(temp.indexOf(" ") + 1) - 32)));
			}
			dir += temp.replaceAll(" ", "");
		}
		dir += ".csv";
		return dir;

	}
}
