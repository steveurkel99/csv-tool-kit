package edu.ucsf.library.idl.csvToolKit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * returns either columns or rows as specified by user
 * to meet certain parameters
 * 
 * @author Michael Antoun
 *
 */

public class Extracter {
	
	/**
	 * 
	 * returns all data in the columns specified by user, 
	 * writes the data to a file
	 * 
	 * @param file
	 * @param columns to be extracted
	 */
	
	public static void getColumns(ArrayList<String> file, String[] cols){
		System.out.println("EXTRACTED DATA:\n");
		Integer[] colNums = findColNumbers(file, cols);	
		for (String line : file){
			//System.out.println(file.indexOf(line) +  ": " + line);
			for (int ind : colNums){
				String s = ErrorCheck.processWords(line.split("\"" + Runner.DELIMETER + "\""))[ind];
				System.out.print(s.substring(1, s.length()-1));
				FileHandler.write("\"" + s.substring(1, s.length()-1) + "\"" + ",", Mode.EXTRACT);
				System.out.print("\t");
			}
			System.out.println();
			FileHandler.write("\r\n", Mode.EXTRACT);
		}
	}
	public static Integer[] findColNumbers(ArrayList<String> file, String[] cols){
		ArrayList<Integer> colNumbers = new ArrayList<Integer>();
		List<String> fields = Arrays.asList(file.get(0).split(Runner.DELIMETER));
		for (String field : fields){
			for (String col : cols){
				if (field.substring(1, field.length()-1).equalsIgnoreCase(col.trim()))
					colNumbers.add(fields.indexOf(field));
			}
		}
		Integer[] a = new Integer[colNumbers.size()];
		return  colNumbers.toArray(a);
	}
	public static void getRecords(ArrayList<String> file, String s){
		String[] pair = s.trim().split("=");
		System.err.println(pair[0]);
		if (!Arrays.asList(file.get(0).toLowerCase().split(Runner.DELIMETER)).contains("\"" + pair[0].toLowerCase() + "\"")){
			System.out.println("No such column exists");
			return;
		}
		String[] a = {pair[0]};
		int ind = findColNumbers(file, a)[0];
		for (String line : file){
			if (ErrorCheck.processWords(line.split("\"" + Runner.DELIMETER + "\""))[ind].equalsIgnoreCase("\"" + pair[1] + "\"")){
				System.out.println(line);
				FileHandler.write(line + "\r\n", Mode.RECORDS);
			}
		}
		
	}
}
