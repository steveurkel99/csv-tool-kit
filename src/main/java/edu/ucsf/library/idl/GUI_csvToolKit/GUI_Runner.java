package edu.ucsf.library.idl.GUI_csvToolKit;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import edu.ucsf.library.idl.csvToolKit.FileHandler;
import edu.ucsf.library.idl.csvToolKit.Middleman;
import edu.ucsf.library.idl.csvToolKit.Runner;

import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;

import javax.swing.JTextField;
import java.awt.GridBagConstraints;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;

public class GUI_Runner extends JFrame {

	private JPanel contentPane;
	private static JTextField txtInputFile;
	private static JTextField txtOutputFile;
	private JTextField txtInputSelected;
	private JTextField txtOutputSelected;
	private static JTextField txtDelimeterInput;
	private static JTextField txtValue;
	private static JTextField txtFields;
	private static JTextArea consoleField;
	private static JRadioButton rdbtnErrorFile, rdbtnCleanFile, rdbtnInformation, rdbtnAllFile, rdbtnFixFile,
			rdbtnVerifyIntegrity, rdbtnPopup, rdbtnExtractColumns, rdbtnExtractRecords, rdbtnSetDelimeter, rdbtnSplitColumn;
	private static JComboBox<String> ColumnsComboBox, RecordsComboBox;
	private static String[] fields = {"Select field"};
	private static JCheckBox checkBoxIn, checkBoxOut;
	private static JComboBox<String> splitComboBox;
	private static JTextField newName1;
	private static JTextField newName2;
	private static JTextField txtSplitDelimeter;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
				try {
					GUI_Runner frame = new GUI_Runner();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI_Runner() {
		ImageIcon img = new ImageIcon(getClass().getResource("images/icon.jpeg"));
		setIconImage(img.getImage());
		setTitle("CSV Tool Kit");
		setBackground(new Color(240, 240, 240));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1500, 800);
		contentPane = new JPanel();
		contentPane.setToolTipText("");
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(15, 15, 15, 15));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 154, 68, 140, 132, 0, 0, 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 26, 0, 67, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 48, 0, 0, 0, 0, 31, 0, 0,
				0, 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
				Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		checkBoxIn = new JCheckBox("");
		checkBoxIn.setBackground(Color.WHITE);
		checkBoxIn.setForeground(Color.BLACK);
		checkBoxIn.setEnabled(false);
		GridBagConstraints gbc_checkBoxIn = new GridBagConstraints();
		gbc_checkBoxIn.insets = new Insets(0, 0, 5, 5);
		gbc_checkBoxIn.gridx = 8;
		gbc_checkBoxIn.gridy = 1;
		contentPane.add(checkBoxIn, gbc_checkBoxIn);

		JLabel lblInputDirectory = new JLabel("Input File");
		lblInputDirectory.setFont(new Font("Tahoma", Font.PLAIN, 20));
		GridBagConstraints gbc_lblInputDirectory = new GridBagConstraints();
		gbc_lblInputDirectory.insets = new Insets(0, 0, 5, 5);
		gbc_lblInputDirectory.gridx = 1;
		gbc_lblInputDirectory.gridy = 1;
		contentPane.add(lblInputDirectory, gbc_lblInputDirectory);

		

		final JButton inBrowseButton = new JButton("Browse");
		inBrowseButton.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_inBrowseButton = new GridBagConstraints();
		gbc_inBrowseButton.insets = new Insets(0, 0, 5, 5);
		gbc_inBrowseButton.gridx = 5;
		gbc_inBrowseButton.gridy = 1;
		contentPane.add(inBrowseButton, gbc_inBrowseButton);
		inBrowseButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				JFileChooser OutputBrowse = new JFileChooser();
				OutputBrowse.setDialogTitle("Output Directory");
				OutputBrowse.setFileSelectionMode(JFileChooser.FILES_ONLY);
				if (OutputBrowse.showOpenDialog(inBrowseButton) == JFileChooser.APPROVE_OPTION) {
					txtInputFile.setText(OutputBrowse.getSelectedFile().getAbsolutePath());
				}
			}

		});

		txtInputSelected = new JTextField();
		txtInputSelected.setEditable(false);
		txtInputSelected.setToolTipText("selected filepath");
		txtInputSelected.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtInputSelected.setColumns(25);
		GridBagConstraints gbc_txtInputSelected = new GridBagConstraints();
		gbc_txtInputSelected.insets = new Insets(0, 0, 5, 5);
		gbc_txtInputSelected.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtInputSelected.gridx = 7;
		gbc_txtInputSelected.gridy = 1;
		contentPane.add(txtInputSelected, gbc_txtInputSelected);

		JButton selectIn = new JButton("Select");
		selectIn.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_selectIn = new GridBagConstraints();
		gbc_selectIn.insets = new Insets(0, 0, 5, 5);
		gbc_selectIn.gridx = 6;
		gbc_selectIn.gridy = 1;
		selectIn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				File f = new File(txtInputFile.getText());
				if (f.exists() && f.isFile()) {
					checkBoxIn.setSelected(true);
					txtInputSelected.setText(txtInputFile.getText());
					Runner.inputDirectory = txtInputSelected.getText();
					String s = Middleman.fileToFixedList(Runner.inputDirectory).get(0);
					fields = s.substring(1, s.length()-1).split("\"" + Runner.DELIMETER + "\"");
					populate(fields);
				}
			}

		});
		contentPane.add(selectIn, gbc_selectIn);

		JLabel lblOutputDirectory = new JLabel(" Output Directory");
		lblOutputDirectory.setFont(new Font("Tahoma", Font.PLAIN, 20));
		GridBagConstraints gbc_lblOutputDirectory = new GridBagConstraints();
		gbc_lblOutputDirectory.insets = new Insets(0, 0, 5, 5);
		gbc_lblOutputDirectory.gridx = 1;
		gbc_lblOutputDirectory.gridy = 2;
		contentPane.add(lblOutputDirectory, gbc_lblOutputDirectory);

		txtInputFile = new JTextField();
		txtInputFile.setToolTipText("Enter Filepath here\r\n");
		txtInputFile.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtInputFile.setColumns(25);
		GridBagConstraints gbc_txtInputFile = new GridBagConstraints();
		gbc_txtInputFile.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtInputFile.gridwidth = 3;
		gbc_txtInputFile.insets = new Insets(0, 0, 5, 5);
		gbc_txtInputFile.gridx = 2;
		gbc_txtInputFile.gridy = 1;
		contentPane.add(txtInputFile, gbc_txtInputFile);
		
		txtOutputFile = new JTextField();
		txtOutputFile.setToolTipText("Enter output directory here\r\n");
		txtOutputFile.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtOutputFile.setColumns(25);
		GridBagConstraints gbc_txtOutputFile = new GridBagConstraints();
		gbc_txtOutputFile.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtOutputFile.gridwidth = 3;
		gbc_txtOutputFile.insets = new Insets(0, 0, 5, 5);
		gbc_txtOutputFile.gridx = 2;
		gbc_txtOutputFile.gridy = 2;
		contentPane.add(txtOutputFile, gbc_txtOutputFile);
		
		checkBoxOut = new JCheckBox("");
		checkBoxOut.setEnabled(false);
		checkBoxOut.setForeground(Color.GREEN);
		checkBoxOut.setBackground(Color.WHITE);
		GridBagConstraints gbc_checkBoxOut = new GridBagConstraints();
		gbc_checkBoxOut.insets = new Insets(0, 0, 5, 5);
		gbc_checkBoxOut.gridx = 8;
		gbc_checkBoxOut.gridy = 2;
		contentPane.add(checkBoxOut, gbc_checkBoxOut);

		final JButton outBrowseButton = new JButton("Browse");
		outBrowseButton.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_btnRun = new GridBagConstraints();
		gbc_btnRun.insets = new Insets(0, 0, 5, 5);
		gbc_btnRun.gridx = 5;
		gbc_btnRun.gridy = 2;
		contentPane.add(outBrowseButton, gbc_btnRun);
		outBrowseButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				JFileChooser inputBrowse = new JFileChooser();
				inputBrowse.setDialogTitle("Input File");
				inputBrowse.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				if (inputBrowse.showOpenDialog(outBrowseButton) == JFileChooser.APPROVE_OPTION) {
					txtOutputFile.setText(inputBrowse.getSelectedFile().getAbsolutePath());
				}
			}

		});

		JButton selectOut = new JButton("Select");
		selectOut.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_selectOut = new GridBagConstraints();
		gbc_selectOut.insets = new Insets(0, 0, 5, 5);
		gbc_selectOut.gridx = 6;
		gbc_selectOut.gridy = 2;
		selectOut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				File f = new File(txtOutputFile.getText());
				if (f.exists() && f.isDirectory()) {
					checkBoxOut.setSelected(true);
					txtOutputSelected.setText(txtOutputFile.getText());
				}

			}

		});
		contentPane.add(selectOut, gbc_selectOut);

		txtOutputSelected = new JTextField();
		txtOutputSelected.setEditable(false);
		txtOutputSelected.setToolTipText("Selected output directory");
		txtOutputSelected.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtOutputSelected.setColumns(25);
		GridBagConstraints gbc_txtOutputSelected = new GridBagConstraints();
		gbc_txtOutputSelected.insets = new Insets(0, 0, 5, 5);
		gbc_txtOutputSelected.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtOutputSelected.gridx = 7;
		gbc_txtOutputSelected.gridy = 2;
		contentPane.add(txtOutputSelected, gbc_txtOutputSelected);

		/*
		 * txtInputSelected = new JTextField();
		txtInputSelected.setEditable(false);
		txtInputSelected.setToolTipText("selected filepath");
		txtInputSelected.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtInputSelected.setColumns(25);
		GridBagConstraints gbc_txtInputSelected = new GridBagConstraints();
		gbc_txtInputSelected.insets = new Insets(0, 0, 5, 5);
		gbc_txtInputSelected.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtInputSelected.gridx = 7;
		gbc_txtInputSelected.gridy = 1;
		contentPane.add(txtInputSelected, gbc_txtInputSelected);
		 * 
		 * 
		 */
		
		
		JLabel lblOptions = new JLabel("OPTIONS");
		lblOptions.setFont(new Font("Tahoma", Font.BOLD, 30));
		GridBagConstraints gbc_lblOptions = new GridBagConstraints();
		gbc_lblOptions.insets = new Insets(30, 0, 5, 5);
		gbc_lblOptions.gridx = 1;
		gbc_lblOptions.gridy = 4;
		contentPane.add(lblOptions, gbc_lblOptions);

		rdbtnErrorFile = new JRadioButton("Error File");
		rdbtnErrorFile.setFont(new Font("Tahoma", Font.BOLD, 16));
		rdbtnErrorFile.setBackground(Color.WHITE);
		GridBagConstraints gbc_rdbtnErrorFile = new GridBagConstraints();
		gbc_rdbtnErrorFile.anchor = GridBagConstraints.WEST;
		gbc_rdbtnErrorFile.insets = new Insets(20, 0, 5, 5);
		gbc_rdbtnErrorFile.gridx = 1;
		gbc_rdbtnErrorFile.gridy = 5;
		contentPane.add(rdbtnErrorFile, gbc_rdbtnErrorFile);

		consoleField = new JTextArea();
		consoleField.setAutoscrolls(true);
		consoleField.setText("OUTPUT");
		consoleField.setEditable(false);
		consoleField.setBackground(Color.BLACK);
		consoleField.setForeground(Color.GREEN);
		consoleField.setFont(new Font("Monospaced", Font.PLAIN, 16));
		consoleField.setWrapStyleWord(true);
		JScrollPane scrollable = new JScrollPane(consoleField);
		GridBagConstraints gbc_txtrOutputGoesHere = new GridBagConstraints();
		gbc_txtrOutputGoesHere.fill = GridBagConstraints.BOTH;
		gbc_txtrOutputGoesHere.gridwidth = 3;
		gbc_txtrOutputGoesHere.gridheight = 12;
		gbc_txtrOutputGoesHere.insets = new Insets(10, 0, 5, 5);
		gbc_txtrOutputGoesHere.gridx = 6;
		gbc_txtrOutputGoesHere.gridy = 4;
		contentPane.add(scrollable, gbc_txtrOutputGoesHere);

		rdbtnCleanFile = new JRadioButton("Clean File");
		rdbtnCleanFile.setHorizontalAlignment(SwingConstants.CENTER);
		rdbtnCleanFile.setFont(new Font("Tahoma", Font.BOLD, 16));
		rdbtnCleanFile.setBackground(Color.WHITE);
		GridBagConstraints gbc_rdbtnCleanFile = new GridBagConstraints();
		gbc_rdbtnCleanFile.anchor = GridBagConstraints.WEST;
		gbc_rdbtnCleanFile.insets = new Insets(10, 0, 5, 5);
		gbc_rdbtnCleanFile.gridx = 1;
		gbc_rdbtnCleanFile.gridy = 6;
		contentPane.add(rdbtnCleanFile, gbc_rdbtnCleanFile);

		rdbtnInformation = new JRadioButton("Information");
		rdbtnInformation.setFont(new Font("Tahoma", Font.BOLD, 16));
		rdbtnInformation.setBackground(Color.WHITE);
		GridBagConstraints gbc_rdbtnInformation = new GridBagConstraints();
		gbc_rdbtnInformation.anchor = GridBagConstraints.WEST;
		gbc_rdbtnInformation.insets = new Insets(10, 0, 5, 5);
		gbc_rdbtnInformation.gridx = 1;
		gbc_rdbtnInformation.gridy = 7;
		contentPane.add(rdbtnInformation, gbc_rdbtnInformation);

		rdbtnAllFile = new JRadioButton("All File");
		rdbtnAllFile.setBackground(Color.WHITE);
		rdbtnAllFile.setFont(new Font("Tahoma", Font.BOLD, 16));
		GridBagConstraints gbc_rdbtnAllFile = new GridBagConstraints();
		gbc_rdbtnAllFile.anchor = GridBagConstraints.WEST;
		gbc_rdbtnAllFile.insets = new Insets(10, 0, 5, 5);
		gbc_rdbtnAllFile.gridx = 1;
		gbc_rdbtnAllFile.gridy = 8;
		contentPane.add(rdbtnAllFile, gbc_rdbtnAllFile);

		rdbtnFixFile = new JRadioButton("Fix File");
		rdbtnFixFile.setFont(new Font("Tahoma", Font.BOLD, 16));
		rdbtnFixFile.setForeground(Color.BLACK);
		rdbtnFixFile.setBackground(Color.WHITE);
		GridBagConstraints gbc_rdbtnFixFile = new GridBagConstraints();
		gbc_rdbtnFixFile.anchor = GridBagConstraints.WEST;
		gbc_rdbtnFixFile.insets = new Insets(10, 0, 5, 5);
		gbc_rdbtnFixFile.gridx = 1;
		gbc_rdbtnFixFile.gridy = 9;
		contentPane.add(rdbtnFixFile, gbc_rdbtnFixFile);

		rdbtnVerifyIntegrity = new JRadioButton("Verify Integrity");
		rdbtnVerifyIntegrity.setFont(new Font("Tahoma", Font.BOLD, 16));
		rdbtnVerifyIntegrity.setBackground(Color.WHITE);
		GridBagConstraints gbc_rdbtnVerifyIntegrity = new GridBagConstraints();
		gbc_rdbtnVerifyIntegrity.anchor = GridBagConstraints.WEST;
		gbc_rdbtnVerifyIntegrity.insets = new Insets(10, 0, 5, 5);
		gbc_rdbtnVerifyIntegrity.gridx = 1;
		gbc_rdbtnVerifyIntegrity.gridy = 10;
		contentPane.add(rdbtnVerifyIntegrity, gbc_rdbtnVerifyIntegrity);

		rdbtnPopup = new JRadioButton("Popup");
		rdbtnPopup.setFont(new Font("Tahoma", Font.BOLD, 16));
		rdbtnPopup.setBackground(Color.WHITE);
		GridBagConstraints gbc_rdbtnPopup = new GridBagConstraints();
		gbc_rdbtnPopup.anchor = GridBagConstraints.WEST;
		gbc_rdbtnPopup.insets = new Insets(10, 0, 5, 5);
		gbc_rdbtnPopup.gridx = 1;
		gbc_rdbtnPopup.gridy = 11;
		contentPane.add(rdbtnPopup, gbc_rdbtnPopup);

		
		
		
		
		
		
		
		
		RecordsComboBox = new JComboBox(fields);
		RecordsComboBox.setForeground(new Color(0, 0, 0));
		RecordsComboBox.setBackground(Color.WHITE);
		RecordsComboBox.setModel(new DefaultComboBoxModel(new String[] {"Select field here      "}));
		RecordsComboBox.setEnabled(false);
		RecordsComboBox.setToolTipText("Select Field Here");
		GridBagConstraints gbc_RecordsComboBox = new GridBagConstraints();
		gbc_RecordsComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_RecordsComboBox.insets = new Insets(10, 0, 5, 5);
		gbc_RecordsComboBox.gridx = 2;
		gbc_RecordsComboBox.gridy = 13;	
		ComboBoxRenderer renderer = new ComboBoxRenderer();
		RecordsComboBox.setRenderer(renderer);
		contentPane.add(RecordsComboBox, gbc_RecordsComboBox);
		
		
		
		
		ColumnsComboBox = new JComboBox(fields);
		ColumnsComboBox.setModel(new DefaultComboBoxModel(new String[] {"Select field here      "}));
		ColumnsComboBox.setBackground(Color.WHITE);
		ColumnsComboBox.setToolTipText("Click on field to add it to list of columns to be extracted");
		ColumnsComboBox.setEnabled(false);
		GridBagConstraints gbc_ColumnsComboBox = new GridBagConstraints();
		gbc_ColumnsComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_ColumnsComboBox.insets = new Insets(10, 0, 5, 5);
		gbc_ColumnsComboBox.gridx = 2;
		gbc_ColumnsComboBox.gridy = 12;
		ColumnsComboBox.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {
				txtFields.setText(txtFields.getText() + ColumnsComboBox.getSelectedItem().toString() + ", ");

			}

		});
		ColumnsComboBox.setRenderer(renderer);
		contentPane.add(ColumnsComboBox, gbc_ColumnsComboBox);

		
		
		
		
		
		
		
		txtFields = new JTextField();
		txtFields.setEnabled(false);
		txtFields.setText("Field1, Field2, Field3");
		txtFields.setToolTipText("insert fields manually or with drop-down menu    **Separated by Comma (,) **\r\n");
		GridBagConstraints gbc_txtFields = new GridBagConstraints();
		gbc_txtFields.gridwidth = 2;
		gbc_txtFields.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtFields.insets = new Insets(10, 0, 5, 5);
		gbc_txtFields.gridx = 4;
		gbc_txtFields.gridy = 12;
		contentPane.add(txtFields, gbc_txtFields);
		txtFields.setColumns(20);

		JLabel label = new JLabel("=");
		label.setFont(new Font("Tahoma", Font.BOLD, 22));
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(10, 0, 5, 5);
		gbc_label.gridx = 3;
		gbc_label.gridy = 13;
		contentPane.add(label, gbc_label);

		txtValue = new JTextField();
		txtValue.setToolTipText("Insert value here");
		txtValue.setEnabled(false);
		txtValue.setText("Value");
		GridBagConstraints gbc_txtValue = new GridBagConstraints();
		gbc_txtValue.gridwidth = 2;
		gbc_txtValue.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtValue.insets = new Insets(10, 0, 5, 5);
		gbc_txtValue.gridx = 4;
		gbc_txtValue.gridy = 13;
		contentPane.add(txtValue, gbc_txtValue);
		txtValue.setColumns(20);

		txtDelimeterInput = new JTextField();
		txtDelimeterInput.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtDelimeterInput.setToolTipText("Delimeter input here");
		txtDelimeterInput.setEnabled(false);
		txtDelimeterInput.setText("Input");
		GridBagConstraints gbc_txtDelimeterInput = new GridBagConstraints();
		gbc_txtDelimeterInput.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtDelimeterInput.insets = new Insets(10, 0, 5, 5);
		gbc_txtDelimeterInput.gridx = 2;
		gbc_txtDelimeterInput.gridy = 14;
		contentPane.add(txtDelimeterInput, gbc_txtDelimeterInput);
		txtDelimeterInput.setColumns(8);

		JButton btnRun = new JButton("CLICK HERE TO RUN");
		btnRun.setToolTipText("runs with selected input");
		btnRun.setForeground(Color.BLACK);
		btnRun.setBackground(Color.RED);
		btnRun.setFont(new Font("Tahoma", Font.BOLD, 30));
		GridBagConstraints gbc_btnRun1 = new GridBagConstraints();
		gbc_btnRun1.gridwidth = 7;
		gbc_btnRun1.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnRun1.insets = new Insets(10, 0, 5, 5);
		gbc_btnRun1.gridx = 1;
		gbc_btnRun1.gridy = 18;
		btnRun.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				runIt();

			}

		});
		
		rdbtnSplitColumn = new JRadioButton("Split Column");
		rdbtnSplitColumn.setBackground(Color.WHITE);
		rdbtnSplitColumn.setFont(new Font("Tahoma", Font.BOLD, 16));
		GridBagConstraints gbc_rdbtnSplitColumn = new GridBagConstraints();
		gbc_rdbtnSplitColumn.anchor = GridBagConstraints.WEST;
		gbc_rdbtnSplitColumn.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnSplitColumn.gridx = 1;
		gbc_rdbtnSplitColumn.gridy = 15;
		rdbtnSplitColumn.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				if (rdbtnSplitColumn.isSelected()) {
					splitComboBox.setEnabled(true);
					txtSplitDelimeter.setEnabled(true);
					txtSplitDelimeter.setText("");
					newName1.setEnabled(true);
					newName1.setText("");
					newName2.setEnabled(true);
					newName2.setText("");
					
				} else {
					txtSplitDelimeter.setEnabled(false);
					splitComboBox.setEnabled(false);
					newName1.setEnabled(false);
					newName2.setEnabled(false);

				}
			}
			}
		
		);
		contentPane.add(rdbtnSplitColumn, gbc_rdbtnSplitColumn);
		
		splitComboBox = new JComboBox<String>(fields);
		splitComboBox.setModel(new DefaultComboBoxModel(new String[] {"Select field here      "}));
		splitComboBox.setSelectedIndex(0);
		splitComboBox.setToolTipText("Click on field to be split");
		splitComboBox.setEnabled(false);
		splitComboBox.setBackground(Color.WHITE);
		GridBagConstraints gbc_splitComboBox = new GridBagConstraints();
		gbc_splitComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_splitComboBox.insets = new Insets(0, 0, 5, 5);
		gbc_splitComboBox.gridx = 2;
		gbc_splitComboBox.gridy = 15;
		contentPane.add(splitComboBox, gbc_splitComboBox);
		
		txtSplitDelimeter = new JTextField();
		txtSplitDelimeter.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtSplitDelimeter.setEnabled(false);
		txtSplitDelimeter.setToolTipText("Enter the delimeter by which to separate the column");
		txtSplitDelimeter.setText("Delimeter");
		GridBagConstraints gbc_txtSplitDelimeter = new GridBagConstraints();
		gbc_txtSplitDelimeter.insets = new Insets(0, 0, 5, 5);
		gbc_txtSplitDelimeter.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtSplitDelimeter.gridx = 3;
		gbc_txtSplitDelimeter.gridy = 15;
		contentPane.add(txtSplitDelimeter, gbc_txtSplitDelimeter);
		txtSplitDelimeter.setColumns(10);
		
		newName1 = new JTextField();
		newName1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		newName1.setEnabled(false);
		newName1.setText("Name 1");
		newName1.setToolTipText("");
		GridBagConstraints gbc_newName1 = new GridBagConstraints();
		gbc_newName1.insets = new Insets(0, 0, 5, 5);
		gbc_newName1.fill = GridBagConstraints.HORIZONTAL;
		gbc_newName1.gridx = 4;
		gbc_newName1.gridy = 15;
		contentPane.add(newName1, gbc_newName1);
		newName1.setColumns(10);
		
		newName2 = new JTextField();
		newName2.setEnabled(false);
		newName2.setText("Name 2");
		newName2.setToolTipText("");
		newName2.setColumns(10);
		GridBagConstraints gbc_newName2 = new GridBagConstraints();
		gbc_newName2.insets = new Insets(0, 0, 5, 5);
		gbc_newName2.fill = GridBagConstraints.HORIZONTAL;
		gbc_newName2.gridx = 5;
		gbc_newName2.gridy = 15;
		contentPane.add(newName2, gbc_newName2);
		contentPane.add(btnRun, gbc_btnRun1);

		rdbtnSetDelimeter = new JRadioButton("Set Delimeter");
		rdbtnSetDelimeter.setFont(new Font("Tahoma", Font.BOLD, 16));
		rdbtnSetDelimeter.setBackground(Color.WHITE);
		GridBagConstraints gbc_rdbtnSetDelimeter = new GridBagConstraints();
		gbc_rdbtnSetDelimeter.anchor = GridBagConstraints.WEST;
		gbc_rdbtnSetDelimeter.insets = new Insets(10, 0, 5, 5);
		gbc_rdbtnSetDelimeter.gridx = 1;
		gbc_rdbtnSetDelimeter.gridy = 14;
		rdbtnSetDelimeter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rdbtnSetDelimeter.isSelected()) {
					txtDelimeterInput.setEnabled(true);
					txtDelimeterInput.setText("");
					txtDelimeterInput.requestFocus();
				} else {
					txtDelimeterInput.setEnabled(false);
				}
			}

		});
		contentPane.add(rdbtnSetDelimeter, gbc_rdbtnSetDelimeter);

		rdbtnExtractRecords = new JRadioButton("Extract Records");
		rdbtnExtractRecords.setFont(new Font("Tahoma", Font.BOLD, 16));
		rdbtnExtractRecords.setBackground(Color.WHITE);
		GridBagConstraints gbc_rdbtnExtractRecords = new GridBagConstraints();
		gbc_rdbtnExtractRecords.anchor = GridBagConstraints.WEST;
		gbc_rdbtnExtractRecords.insets = new Insets(10, 0, 5, 5);
		gbc_rdbtnExtractRecords.gridx = 1;
		gbc_rdbtnExtractRecords.gridy = 13;
		rdbtnExtractRecords.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if (rdbtnExtractRecords.isSelected()) {
					RecordsComboBox.setEnabled(true);
					txtValue.setEnabled(true);
					txtValue.setText("");
					RecordsComboBox.requestFocus();
				} else {
					RecordsComboBox.setEnabled(false);
					txtValue.setEnabled(false);
				}

			}

		});
		contentPane.add(rdbtnExtractRecords, gbc_rdbtnExtractRecords);

		rdbtnExtractColumns = new JRadioButton("Extract Columns");
		rdbtnExtractColumns.setFont(new Font("Tahoma", Font.BOLD, 16));
		rdbtnExtractColumns.setBackground(Color.WHITE);
		GridBagConstraints gbc_rdbtnExtractColumns = new GridBagConstraints();
		gbc_rdbtnExtractColumns.anchor = GridBagConstraints.WEST;
		gbc_rdbtnExtractColumns.insets = new Insets(10, 0, 5, 5);
		gbc_rdbtnExtractColumns.gridx = 1;
		gbc_rdbtnExtractColumns.gridy = 12;
		rdbtnExtractColumns.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if (rdbtnExtractColumns.isSelected()) {
					ColumnsComboBox.setEnabled(true);
					txtFields.setEnabled(true);
					txtFields.setText("");
					ColumnsComboBox.requestFocus();
				} else {
					ColumnsComboBox.setEnabled(false);
					txtFields.setEnabled(false);
				}
			}

		});
		contentPane.add(rdbtnExtractColumns, gbc_rdbtnExtractColumns);

	}

	private static void runIt() {
		String[] arguments = buildArgs();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		System.setOut(new PrintStream(baos));
		try {
			Runner.main(arguments);
		} catch (NullPointerException e) {
			//consoleField.setText("An error has occurred");
			e.printStackTrace();
			return;
		}
		consoleField.setText(baos.toString());
	}

	private static String[] buildArgs() {
		ArrayList<String> stuff = new ArrayList<String>();
		System.out.println(txtOutputFile.getText());
		if (checkBoxIn.isSelected())
			stuff.add("-I" + txtInputFile.getText());
		if (checkBoxOut.isSelected())
			stuff.add("-O" + txtOutputFile.getText());
		
		if (rdbtnErrorFile.isSelected())
			stuff.add("-e");
		if (rdbtnAllFile.isSelected())
			stuff.add("-a");
		if (rdbtnCleanFile.isSelected())
			stuff.add("-c");
		if (rdbtnFixFile.isSelected())
			stuff.add("-f");
		if (rdbtnInformation.isSelected())
			stuff.add("-i");
		if (rdbtnVerifyIntegrity.isSelected())
			stuff.add("-v");
		if (rdbtnPopup.isSelected())
			stuff.add("-p");
		if (rdbtnExtractColumns.isSelected())
			stuff.add("-x" + txtFields.getText());
		if (rdbtnExtractRecords.isSelected()){
			stuff.add("-r");
			stuff.add("\"" + RecordsComboBox.getSelectedItem().toString() + "=" + txtValue.getText() + "\"");
		}
		if (rdbtnSetDelimeter.isSelected())
			stuff.add("-d" + txtDelimeterInput.getText());
		if (rdbtnSplitColumn.isSelected()){
			stuff.add("-s" + splitComboBox.getSelectedItem().toString() + "!" + txtSplitDelimeter.getText() + "!" + newName1.getText() + "!" + newName2.getText());
		}
		
		String[] copy = new String[stuff.size()];
		return stuff.toArray(copy);

	}
	
	public static void populate(String[] collection){
		ColumnsComboBox.setPrototypeDisplayValue("Select  field  here    ");
		RecordsComboBox.setPrototypeDisplayValue("Select  field  here    ");
		splitComboBox.setPrototypeDisplayValue("Select  field  here    ");
		
		if (ColumnsComboBox.getItemCount() == 0)
			ColumnsComboBox.removeAllItems();
		if (RecordsComboBox.getItemCount() == 0)
			RecordsComboBox.removeAllItems();
		if (splitComboBox.getItemCount() == 0)
			splitComboBox.removeAllItems();
		for (String s : collection){
			ColumnsComboBox.addItem(s);
			RecordsComboBox.addItem(s);
			splitComboBox.addItem(s);
		}
	}

	public int getColumnsComboBoxMaximumRowCount() {
		return ColumnsComboBox.getMaximumRowCount();
	}
	public void setColumnsComboBoxMaximumRowCount(int maximumRowCount) {
		ColumnsComboBox.setMaximumRowCount(maximumRowCount);
	}
}



